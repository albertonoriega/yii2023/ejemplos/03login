<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%fotos}}`.
 */
class m230630_040253_create_fotos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%fotos}}', [
            'id' => $this->integer(),
            'nombre' => $this->string(200),
            'noticia' => $this->integer(),
        ]);
        $this->addPrimaryKey("primary", "fotos", 'id');
        $this->addForeignKey('fkfotosNoticias', "fotos", 'noticia', 'noticias', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%fotos}}');
    }
}
