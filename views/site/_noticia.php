<?php

use yii\helpers\Html;
?>
<div class="col-lg-6 mb-3">
    <h2><?= $dato->nombre ?></h2>
    <br>
    <p><?= $dato->descripcion ?></p>
    <br>
    <div class="row">
        <?php
        foreach ($dato->fotos as $foto) {
            echo $this->render("_foto", [
                "foto" => $foto
            ]);
        }
        ?>
    </div>
</div>