<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'fotos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Fotos</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <?php
            foreach ($datos as $dato) {
                echo $this->render("_fotos", [
                    "dato" => $dato
                ]);
            }
            ?>
        </div>
    </div>
</div>