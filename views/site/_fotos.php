<?php

use yii\helpers\Html;
?>
<div class="col-lg-4 mb-3">
    <h2><?= $dato->noticia0->nombre ?></h2>
    <br>
    <div><?= Html::img(
                "@web/imgs/{$dato->nombre}", // ruta  + nombre
                [
                    "class" => 'col-lg-8'
                ] // atributos html
            )
            ?></div>
    <br>

</div>